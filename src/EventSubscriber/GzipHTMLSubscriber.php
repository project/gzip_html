<?php

namespace Drupal\gzip_html\EventSubscriber;

use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\devel\DevelDumperPluginManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Gzips the HTML of the response.
 *
 * @see \Symfony\Component\EventDispatcher\EventSubscriberInterface
 */
class GzipHTMLSubscriber implements EventSubscriberInterface {

  /**
   * The gzip config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Constructs a DevelDumperPluginManager object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->config = $config_factory->get('gzip_html.settings');
  }

  /**
   * Gzips the HTML.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   The filter event.
   */
  public function response(ResponseEvent $event) {
    // Check if compression is disabled.
    if (!$this->config->get('gzip_html')) {
      return;
    }

    $response = $event->getResponse();

    // Make sure that the following render classes are the only ones that
    // are minified.
    $allowed_response_classes = [
      'Drupal\big_pipe\Render\BigPipeResponse',
      'Drupal\Core\Render\HtmlResponse',
    ];
    // ... and if the browser support encoding.
    $return_compressed = isset($_SERVER['HTTP_ACCEPT_ENCODING']) && strpos($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip') !== FALSE;
    if (in_array(get_class($response), $allowed_response_classes)
      && $return_compressed == TRUE
      && $response->getStatusCode() == 200
    ) {
      $content = $response->getContent();
      $compressed = gzencode($content, 9, FORCE_GZIP);
      $response->setContent($compressed);
      $response->headers->set('Content-Encoding', 'gzip');
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [];
    $events[KernelEvents::RESPONSE][] = ['response', -50000];
    return $events;
  }

}
